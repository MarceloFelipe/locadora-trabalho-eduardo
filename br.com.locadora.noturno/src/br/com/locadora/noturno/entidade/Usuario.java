package br.com.locadora.noturno.entidade;



import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUsuario;

	@NotNull(message="Informe o Email")
	private String email;
	@NotNull(message="Informe a Senha")
	private String senha;
	
}
