package br.com.locadora.noturno.controller;

import br.com.locadora.noturno.entidade.*;
import br.com.locadora.noturno.dao.*;

import javax.faces.bean.*;

import java.util.List;

import javax.ejb.*;

import java.io.*;
import javax.faces.context.*;
import javax.servlet.http.*;

@ManagedBean
public class VeiculoRelatorioController {

	@EJB
	private VeiculoDAO veiculoDAO;
	
	private Double precoInicial;
	private Double precoFinal;
	
	private List<Veiculo> veiculos;
	
	public void pdf(){
		
		try{
			
			ByteArrayOutputStream os
			 		= veiculoDAO.pdfPorPreco(precoInicial, precoFinal);
			
			HttpServletResponse response 
					= (HttpServletResponse) FacesContext.getCurrentInstance()
						.getExternalContext()
						.getResponse();
			
			response.setContentType("application/pdf");
			response.setHeader("Content-disposition",
							   "inline;filename=relatorio.pdf");
			response.getOutputStream().write(os.toByteArray());
			
			FacesContext.getCurrentInstance().responseComplete();
			
		}catch(Exception ex){
			
		}
		
	}

	
	public void consultar(){
		this.veiculos 
		  = veiculoDAO.consultaPorPreco(precoInicial, precoFinal);
	}
	
	public List<Veiculo> getVeiculos() {
		return veiculos;
	}



	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}



	public Double getPrecoInicial() {
		return precoInicial;
	}
	public void setPrecoInicial(Double precoInicial) {
		this.precoInicial = precoInicial;
	}
	public Double getPrecoFinal() {
		return precoFinal;
	}
	public void setPrecoFinal(Double precoFinal) {
		this.precoFinal = precoFinal;
	}
	
	
}
