package br.com.locadora.noturno.controller;

import javax.faces.bean.*;
import br.com.locadora.noturno.entidade.*;
import br.com.locadora.noturno.util.Mensagem;
import br.com.locadora.noturno.dao.*;

import java.util.List;

import javax.ejb.*;

@ManagedBean
public class VeiculoController {

	private Veiculo veiculo;
	

	@EJB
	private VeiculoDAO veiculoDAO;
	
	public VeiculoController(){
		this.veiculo = new Veiculo();
	}
	
	public List<Veiculo> consultar(){
		return veiculoDAO.todos();
	}
	
	public void salvar(){
		String erro = veiculoDAO.salvar(veiculo);
		
		if(erro == null){
			Mensagem.sucesso("Salvo!!!");
			this.veiculo = new Veiculo();
		}
		
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	
}
