package br.com.locadora.noturno.controller;

import javax.faces.bean.*;
import br.com.locadora.noturno.entidade.*;
import javax.ejb.*;
import br.com.locadora.noturno.dao.*;
import java.util.*;

@ManagedBean
@RequestScoped
public class OpcionalController {

	@EJB
	private OpcionalDAO opcionalDAO;
	
	public List<Opcional> consultar(){
		return opcionalDAO.todos();
	}
	
}
