package br.com.locadora.noturno.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.locadora.noturno.dao.ClienteDAO;
import br.com.locadora.noturno.dao.LocacaoDAO;
import br.com.locadora.noturno.entidade.Cliente;
import br.com.locadora.noturno.entidade.Locacao;
import br.com.locadora.noturno.entidade.LocacaoVeiculo;


@ManagedBean
public class LocacaoController {
	
	@EJB
	private LocacaoDAO locacaoDAO;
	
	@EJB
	private ClienteDAO clienteDAO;
	
	private Locacao locacao;
	
	private LocacaoVeiculo locacaoVeiculo;
	
	public LocacaoController(){
		this.locacao = new Locacao();
		this.locacaoVeiculo = new LocacaoVeiculo();
	}

	public List<Cliente> todosClientes(){
		return clienteDAO.todos();
	}
	
	public Locacao getLocacao() {
		return locacao;
	}

	public void setLocacao(Locacao locacao) {
		this.locacao = locacao;
	}

	public LocacaoVeiculo getLocacaoVeiculo() {
		return locacaoVeiculo;
	}

	public void setLocacaoVeiculo(LocacaoVeiculo locacaoVeiculo) {
		this.locacaoVeiculo = locacaoVeiculo;
	}
	
	
	
	
}
