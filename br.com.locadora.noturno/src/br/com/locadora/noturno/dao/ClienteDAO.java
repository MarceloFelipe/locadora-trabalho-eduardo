package br.com.locadora.noturno.dao;

import javax.ejb.Stateless;

import br.com.locadora.noturno.entidade.Cliente;

@Stateless
public class ClienteDAO extends GenericDAO<Cliente> {

	public ClienteDAO(){
		super(Cliente.class);
	}
}
