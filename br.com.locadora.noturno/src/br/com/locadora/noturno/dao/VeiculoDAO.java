package br.com.locadora.noturno.dao;

import br.com.locadora.noturno.entidade.*;

import javax.ejb.*;
import java.util.List;
import javax.persistence.*;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.*;

@Stateless
public class VeiculoDAO extends GenericDAO<Veiculo> {

	public VeiculoDAO(){
		super(Veiculo.class);
	}
	
	public ByteArrayOutputStream 
			pdfPorPreco(Double precoInicial, Double precoFinal) throws Exception {
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		Document document = new Document(PageSize.A4.rotate());
		
		PdfWriter writer = PdfWriter.getInstance(document, output);
		
		document.open();
		
		List<Veiculo> veiculos 
			= consultaPorPreco(precoInicial, precoFinal);
		
		Paragraph titulo 
			= new Paragraph("Rel�torio de ve�culos por pre�o");
		titulo.setAlignment(Paragraph.ALIGN_CENTER);
		
		
		document.add(titulo);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		
		PdfPTable tabela = new PdfPTable(4);
		tabela.setWidthPercentage(100);
		tabela.setHeaderRows(1);
		tabela.setWidths(new int[]{10, 20, 35, 35});
		
		String[] cabs = new String[]{"C�d.","Placa","Modelo","Valor"};
		
		for (String cab : cabs) {

			PdfPCell celula = new PdfPCell(new Phrase(cab));
			celula.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			celula.setBackgroundColor(BaseColor.LIGHT_GRAY);
	
			tabela.addCell(celula);
		}
		
		
		for(Veiculo veiculo : veiculos){
			
			PdfPCell cellId 
				= new PdfPCell(new Phrase(veiculo.getIdVeiculo().toString()));
	
			PdfPCell cellPlaca 
				= new PdfPCell(new Phrase(veiculo.getPlaca()));
			
			PdfPCell cellModelo =
					new PdfPCell(new Phrase(veiculo.getModelo().getDescricao()));
		
			PdfPCell cellValor =
					new PdfPCell(new Phrase(veiculo.getValor().toString()));
			
			tabela.addCell(cellId);
			tabela.addCell(cellPlaca);
			tabela.addCell(cellModelo);
			tabela.addCell(cellValor);
		}
		
		
		document.add(tabela);
		
		document.add(Chunk.NEWLINE);
		
		Paragraph resumo 
			= new Paragraph("Total de ve�culos: "+veiculos.size());
		resumo.setAlignment(Element.ALIGN_CENTER);
		
		document.add(resumo);
		
		document.close();
		
		return output;
	}
	
	public List<Veiculo> consultaPorPreco(Double precoInicial, Double precoFinal){
		
		TypedQuery<Veiculo> query 
			= super.em.createQuery("select v from Veiculo as v "
					+ "where v.valor "
					+ "between :precoInicial and :precoFinal",
					Veiculo.class);
		
		//TypedQuery<Veiculo> queryNomeada
		 //	= super.em.createNamedQuery("veiculo.porPreco",
		 //				Veiculo.class);
		
		query.setParameter("precoInicial", precoInicial);
		query.setParameter("precoFinal", precoFinal);
		
		return query.getResultList();
		
	}
}
