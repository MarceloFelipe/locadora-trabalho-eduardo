package br.com.locadora.noturno.dao;

import javax.ejb.Stateless;

import br.com.locadora.noturno.entidade.Locacao;

@Stateless
public class LocacaoDAO extends GenericDAO<Locacao>{
	
	public LocacaoDAO(){
		super(Locacao.class);
	}
}
