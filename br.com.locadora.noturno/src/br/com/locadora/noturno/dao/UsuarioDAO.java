package br.com.locadora.noturno.dao;

import br.com.locadora.noturno.entidade.*;
import javax.ejb.*;

@Stateless
public class UsuarioDAO extends GenericDAO<Usuario> {
	
	public UsuarioDAO(){
		super(Usuario.class);
	}
}
